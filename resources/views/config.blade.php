@extends( "layouts.master")

    @section( "head" )
        <link href="css/config.css" rel="stylesheet" />
    @stop


    @section( "content" )
    <div id="containerConfig">
        <div id="containerConfig_right">
            <div class="boxMenu_active">Configuración</div>
            <div class="boxMenu">Historial de Pedidos</div>
        </div>


        <div id="containerConfig_all">

            <div style="margin-bottom:10px;">
                <div style="margin-bottom:2px;">Nombre de Usuario</div>
                <div>
                    <input style="width:100%;" value="Manolito" />
                </div>
            </div>
            <div style="margin-bottom:10px;">
                <div style="margin-bottom:2px;">Correo electrónico</div>
                <div>
                    <input style="width:100%;" value="manolito@prueba.com" />
                </div>
            </div>
            <div style="margin-bottom:10px;">
                <div style="margin-bottom:2px;">Teléfono</div>
                <div>
                    <input style="width:100%;" value="9024995839" />
                </div>
            </div>

            <div style="margin-bottom:10px;">
                <div style="margin-bottom:2px;">Cambiar Contraseña</div>
                <div style="color:gray;font-size:12px;margin-bottom:3px;">Si se deja en blanco no se cambiará</div>
                <div>
                    <input style="width:100%;" value="" type="password" />
                </div>
            </div>


            <div style="margin-top:25px;text-align:center;"><button>Guardar</button></div>


        </div>

    </div>
    @stop