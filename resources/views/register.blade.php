@extends( "layouts.basic")



@section( "content" )

<div style="display:flex;justify-content:center;padding-top:20px;">
        <div style="box-shadow:3px 3px 3px 3px;width:500px;height:430px;padding:9px;">

                    <h2>Registrar</h2>
                    <form method="POST" action="/register">
                    @csrf <!-- {{ csrf_field() }} -->

                            <div style="margin-bottom:10px;">
                                    <div style="padding-bottom:3px;">Nombre de Usuario</div>
                                    <div>
                                            <input name="usuario" style="width:100%;height:30px;" />
                                    </div>
                            </div>
                            <div style="margin-bottom:10px;">
                                    <div style="padding-bottom:3px;">Correo Electrónico</div>
                                    <div>
                                            <input name="email" style="width:100%;height:30px;" />
                                    </div>
                            </div>

                            <div style="margin-bottom:10px;">
                                    <div style="padding-bottom:3px;">Teléfono</div>
                                    <div>
                                            <input name="telf" style="width:100%;height:30px;" />
                                    </div>
                            </div>


                            <div style="margin-bottom:10px;">
                                    <div style="padding-bottom:3px;">Contraseña</div>
                                    <div>
                                            <input name="pass1" style="width:100%;height:30px;" />
                                    </div>
                            </div>

                            <div style="margin-bottom:10px;">
                                    <div style="padding-bottom:3px;">Vuelve a introducir la contraseña</div>
                                    <div>
                                            <input name="pass2" style="width:100%;height:30px;" />
                                    </div>
                            </div>

                            <div>
                                        <button type="submit">Registrarse</button>

                            </div>
                    </form>
        </div>
</div>



@stop