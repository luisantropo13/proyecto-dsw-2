@extends( "layouts.master" ) 

    @section( "head" )
    <link href="{{asset("css/politicas.css")}}" rel="stylesheet" />
    @stop 


    @section( "content" )

    <script>
                class PoliticasManager 
                {
                        static click( element , id ) 
                        {
                                let headerTabs = document.querySelector("#containerpoliticas_header").querySelectorAll("div");
                                let bodyTabs   = document.querySelector("#politicasContainer").querySelectorAll("div");

                                console.log( headerTabs );
                                for( var i = 0; i < headerTabs.length; i++ ) 
                                {

                                        headerTabs[i].className = ( i == id )  ? "politicasTab_active" : "politicasTab";
                                        bodyTabs[i].style.display = ( i == id ) ? "block" : "none";
                                }
                        }

                }
    </script>


    <div id="containerpoliticas">
        <div>
            <div id="containerpoliticas_header">
                
                    @for( $i = 0; $i < count( $data ) ; $i++  )
                            @if( $i == 0 )
                                     <div class="politicasTab_active" onclick="PoliticasManager.click(this,{{$i}} );">{{$data[$i]->name}}</div>
                            @else 
                                    <div class="politicasTab" onclick="PoliticasManager.click(this,{{$i}} );" >{{$data[$i]->name}}</div>
                            @endif
                    @endfor
            </div>

            <div id="politicasContainer">

                            @foreach( $data as $politica ) 
                                <div>{{$politica->valor}}</div>
                            @endforeach

            </div>
        </div>
    </div>

    @stop

