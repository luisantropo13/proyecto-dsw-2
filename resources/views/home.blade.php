    @extends( "layouts.master" )


    @section("title" , "MajadaMarcial: Inicio")
    @section( "head" )
    
            <link href="/css/index.css" rel="stylesheet" />

            <script src="/js/FormLRManager.js"></script>
            <script src="/js/Visor.js"></script>

    @stop


    @section("content")
    <div id="container">

        <div id="portada"></div>
        
        <div style="display:flex;">
            <div id="visor"></div>
            <div style="flex:1;padding-left:15px;padding-right:5px;display:flex;flex-direction:column;justify-content:center;">
                <div class="infoview">
                    <img src="images/jamburguer.png" />
                    <div>
                        <div class="infoview_title">¡JAMBURGUES!</div>
                        <div>
                            Hacemos las jamburgues mas deliciosas debajo de Fondo de Bikini.
                        </div>
                    </div>
                </div>


                <div class="infoview">
                    <img src="images/jamburguer.png" />
                    <div>
                        <div class="infoview_title">¡JAMBURGUES!</div>
                        <div>
                            Hacemos las jamburgues mas deliciosas debajo de Fondo de Bikini.
                        </div>
                    </div>
                </div>

                <div class="infoview">
                    <img src="images/jamburguer.png" />
                    <div>
                        <div class="infoview_title">¡JAMBURGUES!</div>
                        <div>
                            Hacemos las jamburgues mas deliciosas debajo de Fondo de Bikini.
                        </div>
                    </div>
                </div>

                <div class="infoview">
                    <img src="images/jamburguer.png" />
                    <div>
                        <div class="infoview_title">¡JAMBURGUES!</div>
                        <div>
                            Hacemos las jamburgues mas deliciosas debajo de Fondo de Bikini.
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @stop