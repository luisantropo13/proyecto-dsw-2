@extends( "layouts.admin")


@section( "head" )
    <link href="{{asset("css/admin.index.css")}}" rel="stylesheet" />
@stop 

@section( "content" )




<div style="text-align:center;">

<canvas style="width:90%;background-color:white;height:340px;border:1px solid black;margin-bottom:20px;"></canvas>


<div class="boxInfo" >

    <div style="text-align:center;">
        <img src="{{asset("images/iconproductos.jpeg")}}" class="boxInfo_icon" />
    </div>

    <div style="text-align_center;">
        <div style="font-weight:600;">Productos</div>
        <div style="text-align:center;">{{ $data["num_productos"] }}</div>
    </div>


</div>


<div class="boxInfo" >

    <div style="text-align:center;">
        <img src="{{asset("images/iconproductos.jpeg")}}" class="boxInfo_icon" />
    </div>

    <div style="text-align_center;">
        <div style="font-weight:600;">Usuarios</div>
        <div style="text-align:center;">{{ $data["num_usuarios"] }}</div>
    </div>


</div>

<div class="boxInfo" >

    <div style="text-align:center;">
        <img src="{{asset("images/iconproductos.jpeg")}}" class="boxInfo_icon" />
    </div>

    <div style="text-align_center;">
        <div style="font-weight:600;">Categorías</div>
        <div style="text-align:center;">{{ $data["num_categorias"] }}</div>
    </div>


</div>
</div>

@stop
