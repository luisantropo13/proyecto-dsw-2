<footer>
    <div id="footer_header">
        <div>
            Horarios 9:00-12:00 y de 18:00-20:00
        </div>

        <div>
            <img src="images/icon_fb.png" class="iconRed" />
        </div>
    </div>

    <div id="footer_container">
        <ul>
            <li>Empresas</li>

            <li>Algo1</li>
            <li>Algo2</li>
            <li>Algo3</li>

        </ul>

        <ul>
            <li>Redes Sociales</li>

            <li>Facebook</li>
            <li>Instagram</li>
            <li>Twitter</li>
            <li>Tuenti</li>
        </ul>

    </div>

    <div id="footer_copy">
        Página desarrollada por Luis A. Cabrera Santana y Aridany Gonsalez Granjaesponjeadora. 2021 &copy;
    </div>


</footer>