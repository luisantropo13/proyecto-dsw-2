<header>
		<div>
			<h1>Comida MajadaMarcial</h1>
		</div>

		<nav>
			<div class="active"><a href="./">Inicio</a></div>
			<div><a href="./menu">Productos</a></div>
			<div><a href="./nosotros">Nosotros</a></div>
			<div><a href="./legal">Políticas</a></div>


			@if( session()->has("user"))
			<div><a href="./config">Configuración</a></div>
			@endif

		</nav>




</header>

	<div id="listBuy" style="display:none;">
		<div>Carrito de compra</div>
		<div>
				<div class="listBuy_item">
					<div>
						<img src="images/jamburguer.jpeg" />
					</div>

					<div>
						<div style="font-weight:600;">Jamburguer</div>
						<div style="text-align:center;">x2</div>	
					</div>

					<div>
						<img src="images/papelera.png" style="width:30px;height:35px;" />	
					</div>

				</div>	
		</div>
	</div>

	<div id="iconBuy" onclick=" a = document.querySelector('#listBuy'); a.style.display = ( a.style.display == 'flex') ? 'none' : 'flex'; ">
			<img src="images/carrito.png" />
	</div>
