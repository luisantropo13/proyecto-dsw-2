@extends( "layouts.master" )


    @section( "head" )
    <link href="{{asset("css/menu.css")}}" rel="stylesheet" />


    <script>
                    function envio( form ) 
                    {   
                            document.location = document.querySelector("#filter_cat").value ;
                            event.preventDefault();
                    }
    </script>

    @stop

    @section( "content" )
    <form method="get" onsubmit="envio( this );" id="boxSearch">
        <div style="display:flex;height:50px;">

                <select id="filter_cat" style="height:100%;flex:1;">
                            @foreach($categorias as $categoria)
                                <option value="{{$categoria->id}}">Categoría: {{$categoria->nombre}}</option>
                            @endforeach
                </select>
        </div>

        <button type="submit">Buscar</button>

    </form>



    <div id="containerProducts">


        @foreach( $productos as $producto )
                    <div class="producto">
                        <div>
                            <div class="boxProducto_price">{{$producto->precio}}€</div>
                            <img src="{{$producto->imagen}}" class="producto_image" />
                        </div>
                        <div>
                            <div class="producto_text">{{$producto->nombre}}</div>
                            <div class="producto_boxinfo">
                                <div style="margin-bottom:10px;">
                                    <fieldset>
                                        <legend>Cantidad</legend>
                                        <select>
                                                    @for( $i = 1 ; $i < 11; $i++ ) 
                                                        <option>{{$i}}</option>
                                                    @endfor
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="producto_buttons">
                                    <button>Comprar</button>
                                    <button>Información</button>
                                </div>
                            </div>
                        </div>
                    </div>
        @endforeach



    </div>
    @stop