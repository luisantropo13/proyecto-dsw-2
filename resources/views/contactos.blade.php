@extends( "layouts.master")

    @section( "head" )
    <link href="css/nosotros.css" rel="stylesheet" />
    @stop

    @section( "content" )
    <div id="containerNosotros">
        <div>
            <div id="containerNosotros_left">
                {{ $texto_nosotros }}
                <div id="containerForm">
                    <form method="get" enctype="text/plain" action="mailto:email@algo.com">
                        <div>
                            <div>Email</div>
                            <input type="email" />
                        </div>

                        <div>
                            <div>Descripcion</div>
                            <textarea>

										</textarea>
                        </div>

                        <div>
                            <input type="submit" value="Enviar" />
                        </div>

                    </form>

                </div>
            </div>
        </div>

        <div style="flex:1;">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3505.9262780612885!2d-13.869260584920102!3d28.51186568246553!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2ses!4v1614926904767!5m2!1ses!2ses" style="height:100%;width:100%;border:0;" allowfullscreen="" loading="lazy"></iframe>
        </div>


    </div>
    @stop
