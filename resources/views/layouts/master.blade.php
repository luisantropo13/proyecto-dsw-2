<!DOCTYPE html>
<html lang="es">
<head>

    <meta charset="UTF-8" />
    <title>@yield("title")</title>
    <link href="/css/basic.css" rel="stylesheet" />
    @yield("head")

</head>

<body>
    @include( "partials.header" )

    @yield("content")

    @include( "partials.footer" )


</body>
</html>