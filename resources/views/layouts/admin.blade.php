<!DOCTYPE html>
<html lang="es">

<head>
    <link href="{{asset("css/basic.css")}}" rel="stylesheet" />
    <script src="{{asset("js/ImageManager.js")}}"></script>
    <link href="{{asset("css/admin.css")}}" rel="stylesheet" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield("head")
</head>

<body>

    <div style="flex:1;display:flex;">
        <div style="background-color:#454545;width:265px;border-right: 1px solid #80808040;">
            <div id="title" style="margin-bottom:20px;height:40px;text-align:center;padding-top:10px;">
                <h1 style="color:white;font-size:24px;font-weight:500;">Panel Administrador</h1>
            </div>


        <!-- menu -->


            <div class="panel_menu">Inicio</div>
            <a class="panel_menu_item" href="index">Inicio</a>


            <div class="panel_menu">Tienda</div>
            <a class="panel_menu_item" href="productos">Productos</a>



            <div class="panel_menu">Administrar</div>
            <a class="panel_menu_item" href="usuarios">Usuarios</a>

        <!-- menu -->

        </div>

        <div style="flex-direction:column; display:flex; flex:1;">
            <header style="display:flex;background-color:#00a7d3;height:40px;">
                <div style="flex:1;">

                </div>
                <div style="flex:auto;text-align:right;">
                    <img style="width:35px;height:35px;vertical-align:middle;" src="https://image.flaticon.com/icons/png/128/2416/2416668.png" />
                    JuanPepe
                </div>
            </header>

            <div id="container" style="max-height: calc( 100vh - 40px ) ;background-color:#e1e3e3;flex:1;overflow-y:auto;">
                @yield("content")
            </div>
        </div>
    </div>

        @include( "partials.windowImage")

</body>

</html>