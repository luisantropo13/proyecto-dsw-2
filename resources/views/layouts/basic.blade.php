<!DOCTYPE html>
<html lang="es">
<head>

    <meta charset="UTF-8" />
    <title>@yield("title")</title>
    <link href="/css/basic.css" rel="stylesheet" />
    @yield("head")

</head>

<body>

    @yield("content")

</body>
</html>