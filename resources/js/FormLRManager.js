// Form Manager Login and Register.
class FormLRManager {
    static getFormRegister() {
        return {
            fields: {
                usuario: document.querySelector("#register_usuario"),
                email: document.querySelector("#register_email"),
                pass: document.querySelector("#register_pass"),
                pass2: document.querySelector("#register_pass2"),
            },
        };
    }

    static register() {
        const FORM_REGISTER = FormLRManager.getFormRegister();

        const FORM = new FormData();
        FORM.append("usuario", FORM_REGISTER.fields.usuario);
        FORM.append("pass", FORM_REGISTER.fields.pass);
        FORM.append("email", FORM_REGISTER.fields.email);
        FORM.append("pass2", FORM_REGISTER.fields.pass2);

        let ajax = new XMLHttpRequest();
        ajax.open("POST", "register", true);

        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4) {
                try {
                    let response = JSON.parse(ajax.responseText);

                    switch (0x00) {
                    }
                } catch (Exception) {}
            }
        };

        ajax.send(FORM);
    }
}
