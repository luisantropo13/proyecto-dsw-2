drop database cafeteria;
create database if not exists cafeteria;
use cafeteria;

create table categorias(
id_categoria int unsigned unique primary key auto_increment,
nombre varchar(25) not null,
habilitado boolean 
);

create table clientes(
id_cliente int unsigned unique primary key auto_increment,
nombre varchar(45) not null,
adminis boolean,
email varchar(45) not null,
password varchar(45) not null,
phone varchar(12),
curso enum("DAW","ASIR","DAM")
);

create table vari(
id_vari int unsigned unique primary key auto_increment,
clave_vari varchar(45),
valor int unsigned
);

create table imgs(
id_img int unsigned unique primary key auto_increment,
ruta varchar(45)
);

create table productos(
id_producto int unsigned unique primary key auto_increment,
nombre varchar(45),
fk_img int unsigned,
fk_cate int unsigned,
precio float,
stock int unsigned,
descripcion text,
habilitadp boolean,
foreign key (fk_img) references imgs (id_img),
foreign key (fk_cate) references categorias (id_categoria)
);

create table pedidos(
fk_cliente int unsigned,
fk_producto int unsigned,
cantidad int default 1,
fecha date,
estado enum ("No ha empezado","En proceso","A la espera de recogida"),
foreign key (fk_cliente) references clientes (id_cliente),
foreign key (fk_producto) references productos (id_producto),
primary key(fk_cliente,fk_producto,fecha)
);

create table hisorial(
id_historial int unsigned unique primary key auto_increment,
nombre_cliente varchar(45) not null,
producto float,
cantidad int unsigned,
fecha date,
cantidad_ganada float
);