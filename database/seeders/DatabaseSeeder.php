<?php

namespace Database\Seeders;

use Database\Factories\ClienteFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ClientesTableSeeder::class);
        $this->call(CategoriasTableSeeder::class);
        $this->call(FotosTableSeeder::class);
        $this->call(ProductosTableSeeder::class);
        $this->call(VarisTableSeeder::class);
        $this->call(PoliticasSeeder::class);
    }
}
