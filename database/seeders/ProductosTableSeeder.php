<?php

namespace Database\Seeders;
use App\Models\Producto;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class ProductosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Producto::Create(['nombre'=>'Bebida de ejemplo 1','fk_foto'=>'6','fk_categoria'=>'1','precio'=>'1','stock'=>'20','description'=>'Bebida de ejemplo']);
        Producto::Create(['nombre'=>'Bebida de ejemplo 2','fk_foto'=>'7','fk_categoria'=>'1','precio'=>'1','stock'=>'20','description'=>'Bebida de ejemplo']);
        Producto::Create(['nombre'=>'Bebida de ejemplo 3','fk_foto'=>'8','fk_categoria'=>'1','precio'=>'1','stock'=>'20','description'=>'Bebida de ejemplo']);
        Producto::Create(['nombre'=>'Burger de ejemplo 1','fk_foto'=>'9','fk_categoria'=>'2','precio'=>'1','stock'=>'20','description'=>'Burger de ejemplo']);
        Producto::Create(['nombre'=>'Burger de ejemplo 2','fk_foto'=>'10','fk_categoria'=>'2','precio'=>'1','stock'=>'20','description'=>'Burger de ejemplo']);
        Producto::Create(['nombre'=>'Burger de ejemplo 3','fk_foto'=>'11','fk_categoria'=>'2','precio'=>'1','stock'=>'20','description'=>'Burger de ejemplo']);
        Producto::Create(['nombre'=>'Bocadillo de ejemplo 1','fk_foto'=>'3','fk_categoria'=>'3','precio'=>'1','stock'=>'20','description'=>'bocadillo de ejemplo']);
        Producto::Create(['nombre'=>'Bocadillo de ejemplo 2','fk_foto'=>'4','fk_categoria'=>'3','precio'=>'1','stock'=>'20','description'=>'bocadillo de ejemplo']);
        Producto::Create(['nombre'=>'Bocadillo de ejemplo 3','fk_foto'=>'5','fk_categoria'=>'3','precio'=>'1','stock'=>'20','description'=>'bocadillo de ejemplo']);
        Producto::Create(['nombre'=>'Fruta de ejemplo 1','fk_foto'=>'12','fk_categoria'=>'4','precio'=>'1','stock'=>'20','description'=>'Fruta de ejemplo']);
        Producto::Create(['nombre'=>'Fruta de ejemplo 2','fk_foto'=>'13','fk_categoria'=>'4','precio'=>'1','stock'=>'20','description'=>'Fruta de ejemplo']);
        Producto::Create(['nombre'=>'Fruta de ejemplo 3','fk_foto'=>'14','fk_categoria'=>'4','precio'=>'1','stock'=>'20','description'=>'Fruta de ejemplo']);
        Producto::Create(['nombre'=>'Snack de ejemplo 1','fk_foto'=>'15','fk_categoria'=>'5','precio'=>'1','stock'=>'20','description'=>'Snack de ejemplo']);
        Producto::Create(['nombre'=>'Snack de ejemplo 2','fk_foto'=>'16','fk_categoria'=>'5','precio'=>'1','stock'=>'20','description'=>'Snack de ejemplo']);
        Producto::Create(['nombre'=>'Snack de ejemplo 3','fk_foto'=>'17','fk_categoria'=>'5','precio'=>'1','stock'=>'20','description'=>'Snack de ejemplo']);
        
    }
}
