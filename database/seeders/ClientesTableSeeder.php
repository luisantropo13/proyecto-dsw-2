<?php

namespace Database\Seeders;

use App\Models\Cliente;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clientes')->delete();
        Cliente::create(['nombre' => 'Luis', 'email' => 'luis@luis.es', 'pass' => '1234', 'phone' => '928111111', 'adminis' => 0 ]);
        Cliente::factory()->count(5)->create();
    }
}
