<?php

namespace Database\Seeders;
use App\Models\Politica;
use Illuminate\Database\Seeder;

class PoliticasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Politica::Create(['name'=>'Politica de Cookies','valor'=>' A continuación se identifican las cookies que están siendo utilizadas en este portal así como su tipología y función:

            La página web del Ministerio del Interior utiliza Google Analytics, un servicio de analítica web desarrollada por Google, que permite la medición y análisis de la navegación en las páginas web. En su navegador podrá observar cookies de este servicio. Según la tipología anterior se trata de cookies propias, de sesión y de análisis.
            
            A través de la analítica web se obtiene información relativa al número de usuarios que acceden a la web, el número de páginas vistas, la frecuencia y repetición de las visitas, su duración, el navegador utilizado, el operador que presta el servicio, el idioma, el terminal que utiliza y la ciudad a la que está asignada su dirección IP. Información que posibilita un mejor y más apropiado servicio por parte de este portal.
            
            Para garantizar el anonimato, Google convertirá su información en anónima truncando la dirección IP antes de almacenarla, de forma que Google Analytics no se usa para localizar o recabar información personal identificable de los visitantes del sitio. Google solo podrá enviar la información recabada por Google Analytics a terceros cuanto esté legalmente obligado a ello. Con arreglo a las condiciones de prestación del servicio de Google Analytics, Google no asociará su dirección IP a ningún otro dato conservado por Google.
            
            Otra de las cookies que se descargan es una cookie de tipo técnico denominada JSESSIONID. Esta cookie permite almacenar un identificador único por sesión a través del que es posible vincular datos necesarios para posibilitar la navegación en curso.
            
            Por último, se descarga una cookie denominada show_cookies, propia, de tipo técnico y de sesión. Gestiona el consentimiento del usuario para el uso de las cookies en la página web, con el objeto de recordar aquellos usuarios que las han aceptado y aquellos que no, de modo que a los primeros no se les muestre información en la parte superior de la página al respecto.']);
        
            Politica::Create(['name'=>'Politica de Uso de Datos','valor'=>'

            En cumplimiento con lo establecido en la ley y de conformidad con el Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016 relativo a la protección de las personas fiósicas en lo que respecta al tratamiento de datos personales y a la libre circulación de estos datos, y a Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal , le informamos que sus datos personales serán tratados y quedarán incorporados en ficheros responsabilidad de la Fundación Formación y Empleo Miguel Escalera FOREM registrados en la Agencia Española de Protección de Datos, con la finalidad de tramitar y gestionar su solicitud en relación con los diferentes servicios ofertados por la Fundación en este portal.
            
            Los datos obligatorios resultan necesarios para ofrecerle un correcto servicio, de manera que de no facilitarlos no será posible atenderle. Usted consiente expresamente en la recogida y el tratamiento de todos los datos que nos facilite para la citada finalidad.
            
            Se permite la reproducción, transmisión o comunicación pública del contenido de este Portal únicamente con fines informativos o divulgativos, siempre que se cite la fuente (nombre del autor, URL de la web) se respete su autoría y previa autorización por escrito de FOREM.
            
            En todo caso, puede ejercitar los derechos de acceso, rectificación, cancelación y oposición de los datos personales recogidos en el fichero, enviando un escrito dirigido a FOREM Protección de Datos, Calle Arenal , 11, 1ª planta 28013 Madrid o a la dirección de correo electrónico lopd@forem.ccoo.es.
            
            Le rogamos que en el supuesto de producirse alguna modificación en sus datos de carácter personal, nos lo comunique con el fin de mantener actualizados los mismos.
            
            Si tiene alguna duda al respecto puede remitir un correo electrónico a lopd@forem.ccoo.es
            
            Asimismo, salvo que manifieste lo contrario, sus datos podrán ser empleados por dicha Fundación para remitirle información sobre los servicios de orientación, asesoramiento, formación e información personal y profesional que promueve/desarrolla FOREM.
            ']);
    }
}
