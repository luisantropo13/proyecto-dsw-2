<?php

namespace Database\Seeders;
use App\Models\Vari;
use Illuminate\Database\Seeder;

class VarisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Vari::Create(["clave"=>"text_footer", "valor"=>"Horarios 9:00-12:00 y de 18:00-20:00"]);
        Vari::Create(["clave"=>"text_contact", "valor"=>"qwerty@qwert.com"]);
        
    }
}
