<?php

namespace Database\Seeders;

use App\Models\Categoria;
use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categoria::create(['nombre' => 'Bebida']);
        Categoria::create(['nombre' => 'Burger']);
        Categoria::create(['nombre' => 'Bocadillo']);
        Categoria::create(['nombre' => 'Fruta']);
        Categoria::create(['nombre' => 'Snacks']);
        Categoria::create(['nombre' => 'Otros']);
    }
}
