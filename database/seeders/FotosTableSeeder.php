<?php

namespace Database\Seeders;

use App\Models\Foto;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FotosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Foto::Create(['name' => 'Error_404', 'ruta' => 'https://bigseoagency.com/wp-content/uploads/2018/03/error-404-foxplay-1280x720.png']);
        Foto::Create(['name' => 'Error_500', 'ruta' => 'https://www.neolo.com/blog/wp-content/uploads/2019/10/que-es-error-500.jpg']);
        Foto::Create(['name' => 'Bocadillo de ejemplo', 'ruta' => 'https://vod-hogarmania.atresmedia.com/hogarmania/images/images01/2020/01/22/5e28232e1f4daa0001932b1e/1239x697.jpg']);
        Foto::Create(['name' => 'Bocadillo de ejemplo 2', 'ruta' => 'https://img-global.cpcdn.com/recipes/55a71c97d4fecdb9/1200x630cq70/photo.jpg']);
        Foto::Create(['name' => 'Bocadillo de ejemplo 3', 'ruta' => 'https://www.isabel.net/wp-content/uploads/2019/06/bocadillo-vegetal1920x1080.jpg']);
        Foto::Create(['name' => 'Bebida de ejemplo', 'ruta' => 'https://s1.eestatic.com/2018/09/24/cocinillas/cocinillas_340480272_116593014_1600x938.jpg']);
        Foto::Create(['name' => 'Bebida de ejemplo 2', 'ruta' => 'https://images-na.ssl-images-amazon.com/images/I/81I-gVmFGKL._AC_SL1500_.jpg']);
        Foto::Create(['name' => 'Bebida de ejemplo 3', 'ruta' => 'https://www.losultramarinos.org/Imagenes/tienda/Productos/Original/303_00.jpg']);
        Foto::Create(['name' => 'Burger de ejemplo', 'ruta' => 'https://img.interempresas.net/fotos/1622791.jpeg']);
        Foto::Create(['name' => 'Burger de ejemplo 2', 'ruta' => 'https://controlpublicidad.com/uploads/2019/11/fondue-burger-extra-081233.jpg']);
        Foto::Create(['name' => 'Burger de ejemplo 3', 'ruta' => 'https://i.ytimg.com/vi/dw9Ea-QeC1Q/maxresdefault.jpg']);
        Foto::Create(['name' => 'Fruta de ejemplo', 'ruta' => 'https://img.huffingtonpost.com/asset/5ef9ffab250000a502c28ec2.jpeg?ops=scalefit_720_noupscale']);
        Foto::Create(['name' => 'Fruta de ejemplo 2', 'ruta' => 'https://imagenes.20minutos.es/files/image_656_370/uploads/imagenes/2020/07/23/propiedades-de-la-pera.jpeg']);
        Foto::Create(['name' => 'Fruta de ejemplo 3', 'ruta' => 'https://www.hogarmania.com/archivos/201906/naranja-vitaminac-1280x720x80xX.jpg']);
        Foto::Create(['name' => 'Snack de ejemplo', 'ruta' => 'https://i.ytimg.com/vi/R7FmI0WJ94c/maxresdefault.jpg']);
        Foto::Create(['name' => 'Snack de ejemplo 2', 'ruta' => 'https://www.lacasadelasgolosinas.com/3955/ramo-de-chupachups.jpg']);
        Foto::Create(['name' => 'Snack de ejemplo 3', 'ruta' => 'https://cdn.computerhoy.com/sites/navi.axelspringer.es/public/styles/1200/public/media/image/2020/04/bolsas-patatas-fritas-supermercado-1931649.jpg?itok=XX_eBSR4']);
    }
}
