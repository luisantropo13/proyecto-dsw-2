<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->string('nombre',32);
            $table->unsignedBigInteger('fk_foto');
            $table->unsignedBigInteger('fk_categoria');
            $table->integer('precio');
            $table->integer('stock');
            $table->text('description');
            $table->foreign('fk_foto')->references('id')->on('fotos')->onDelete('cascade');
            $table->foreign('fk_categoria')->references('id')->on('categorias')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
