<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->unsignedBigInteger('fk_cliente');
            $table->unsignedBigInteger('fk_producto');
            $table->integer('cantidad');
            $table->date('fecha');
            $table->enum("estado",['No_ha_empezado','En_proceso','A_la_espera_de_recogida']);
            $table->foreign('fk_cliente')->references('id')->on('clientes')->onDelete('cascade');
            $table->foreign('fk_producto')->references('id')->on('productos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
