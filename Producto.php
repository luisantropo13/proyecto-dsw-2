<?php

use App\Models\Menu;
class Producto 
{
    private $nombre;
    private $precio;
    private $stock;
    private $description;
    private $imagen;
    public function __construct($valorObjeto)
    {
        $this->nombre=$valorObjeto->nombre;
        $this->precio=$valorObjeto->precio;
        $this->stock=$valorObjeto->stock;
        $this->description=$valorObjeto->description;
        $this->imagen=Menu::getImagenProducto($valorObjeto->fk_foto);
    }

    public function getNombre( ) { return $this->nombre; }
    public function getPrecio( ) { return $this->precio; }
    public function getStock( ) { return $this->stock; }
    public function getDescription ( ) { return $this->description; }
    public function getImagen( ) { return $this->imagen; }

}
