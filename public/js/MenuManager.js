class MenuManager {
    static menus = [];
    static indices = [ ];
    static index = 1;


    static getIndices( ) 
    {
            let r = MenuManager.indices.toString();

            return ( r == "" ) ? 0 : r;
    }

    static loadProductos( numMax ) 
    {
        let optionsData = "";
        for( var i = 1; i < 11; i++ ) 
        {
            optionsData += `<option>${i}</option>`;
        }

        let ajax = new XMLHttpRequest();
                ajax.open("get" , "/menu/"+numMax+"/"+MenuManager.getIndices() , true );
                ajax.onreadystatechange = function( ) 
                {
                        if( ajax.readyState == 4 )
                        {
                                    let val = JSON.parse( ajax.responseText );
                                    let padre = document.querySelector("#containerProducts");
                                    

                                       

                                   for( var i = MenuManager.index; i < val.length; i++)
                                   {
                                        const ESTO = val[i];


                                        padre.innerHTML += `

                                                    <div class="producto">
                                                    <div>
                                                        <div class="boxProducto_price">${ESTO.original.precio}€</div>
                                                        <img src="${ESTO.path}" class="producto_image" />
                                                    </div>
                                                    <div>
                                                        <div class="producto_text">${ESTO.original.nombre}</div>
                                                        <div class="producto_boxinfo">
                                                            <div style="margin-bottom:10px;">
                                                                <fieldset>
                                                                    <legend>Cantidad</legend>
                                                                    <select>
                                                                            ${optionsData}
                                                                    </select>
                                                                </fieldset>
                                                            </div>
                                                            <div class="producto_buttons">
                                                                <button>Comprar</button>
                                                                <button>Información</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                        `;

                                    };


                                    MenuManager.index = numMax;

                        }
                }

            ajax.send( null );
    }


    static setMenu(name) {
        for (var i = 0; i < MenuManager.menus.length; i++) {
            if (MenuManager.menus[i] == name) {
                MenuManager.menus.splice(i, 1);
                document.querySelector("#box_" + name).style.borderColor =
                    "gray";
                return true;
            }
        }

        MenuManager.menus.push(name);
        document.querySelector("#box_" + name).style.borderColor = "red";

        MenuManager.indices.push( document.querySelector("#box_" + name).dataset.id_producto );

        return false;
    }
}

window.addEventListener("load", function () {
    document.querySelectorAll(".boxCat").forEach(function (val) {
        val.addEventListener("click", function () {
            MenuManager.setMenu(this.innerHTML);
        });
    });
});

window.addEventListener("scroll" , function() 
{
    if(event.target.scrollTop === event.target.scrollTopMax){
        MenuManager.loadProductos( MenuManager.index + 4 );
  }
});
