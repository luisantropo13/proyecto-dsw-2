class ImageManager {
    static requestImage(callback) {

        let formulario = new FormData();
            formulario.append( "_token" , document.getElementsByName("csrf-token")[0].content );

        let ajax = new XMLHttpRequest();
        ajax.open("POST", "/admin/images", true);
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4) {
                try {
                    let images = JSON.parse(ajax.responseText);

                    document.querySelector("#windowImage").style.display = (images.length > 0) ? "flex" : "none";

                    images.forEach((val) => {
                        const imagen = document.createElement("img");
                        imagen.src = val.ruta;
                        imagen.style = "width:200px;height:200px;margin:5px;";
                        imagen.onclick = function() {
                            callback({
                                status: "ok",
                                value: { path : val.ruta , id_image : val.id }
                            });
                            document.querySelector("#windowImage").style.display = "none";
                        }

                        document.querySelector("#windowImageContainer").appendChild(imagen);
                    });

                } catch (e) {
                    callback({
                        status: "error",
                        value: e
                    });
                }
            }
        }

        ajax.send(formulario);
    }


};