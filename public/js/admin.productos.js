
			function createElement( ele , data ) 
			{
				let c = document.createElement( ele );

				if( typeof data == "object" ) 
					c.appendChild( data );
				else
					c.innerHTML = data;

				return c;
			}

			class Producto
			{
				constructor( data ) 
				{
					this.id     = data.id;
					this.nombre = data.nombre;
					this.precio = data.precio;
					this.stock  = data.stock;
					this.imagen = data.image;
					this.categoria = data.categoria;
					this.descripcion = data.descripcion;
				}

				getRow( ) 
				{
					const ESTO = this;
					let tr = document.createElement( "tr" );
					this.tr = tr;
					let button = createElement( "button" , "Editar" );
					    button.onclick = function( ) 
					    {
							ManagerWindow.editProducto( ESTO );	
					    }
					let button_del = createElement( "button" ,"Eliminar" );
						button_del.onclick = function( ) 
						{
							ManagerWindow.removeProducto( ESTO );
						}

					let tds = [
							this.td_id          = createElement( "td" , this.id ),
							this.td_nombre      = createElement( "td" , this.nombre ),
							this.td_descripcion = createElement( "td" , this.descripcion ),
							this.td_precio      = createElement( "td" , this.precio ),
							this.td_categoria   = createElement( "td" , this.categoria ),
							this.td_stock       = createElement( "td" , this.stock ),
							this.td_button      = createElement( "td" , button ),
							this.td_button_del  = createElement( "td" , button_del )
					];

					tds.forEach( ( val ) => tr.appendChild( val ) );

					return tr;
				}

				getContent() { return this.tr; }

				setNombre( val ) 
				{
					this.td_nombre.innerHTML = val;
					this.nombre              = val;
				}

				setDescripcion( val ) 
				{
					this.td_descripcion.innerHTML = val;
					this.descripcion              = val;
				}


				setPrecio( val ) 
				{
					this.td_precio.innerHTML = val;
					this.precio              = val;
				}


				setStock( val ) 
				{
					this.td_stock.innerHTML  = val;
					this.stock              = val;
				}
			};

			class ManagerWindow
			{
				static actualProduct = null;
				static event;

				static getForm( ) 
				{
					return  {
							money : document.querySelector("#producto_edit_money"),
							name  : document.querySelector("#producto_edit_nombre"),
							text  : document.querySelector("#producto_edit_text"),	
							stock : document.querySelector("#producto_edit_stock")	,
							image : document.querySelector("#producto_edit_image")
					};

				}

				static removeProducto( obj)
				{
					fetch( `/admin/productos/delete/${obj.id}`)
						.then( ( ) => obj.getContent().parentNode.removeChild( obj.getContent()  ) );
				}


				static openWindow( event , producto ) 
				{
						switch( event ) 
						{
							case "edit" :  ManagerWindow.editProducto( producto );
							break;
							case "create":  ManagerWindow.createProducto( );
							break;
						}
				}

				static createProducto( ) 
				{
					ManagerWindow.event = "create";
					let campos = ManagerWindow.getForm( );	
					campos.money.value = "";
					campos.name.value = "";
					campos.text.value = "";
					campos.stock.value = "";
					campos.image.src = "";
					document.querySelector("#windowForm").style.display = "flex";

				}

				static editProducto( producto ) 
				{
					ManagerWindow.event = "edit";
					ManagerWindow.actualProduct = producto;
			
					let campos = ManagerWindow.getForm( );	

					campos.money.value = producto.precio;
					campos.name.value  = producto.nombre;
					campos.text.value  = producto.descripcion;
					campos.stock.value = producto.stock;
					campos.image.src   = producto.imagen;

					document.querySelector("#windowForm").style.display = "flex";
				}

				static clickImage( image ) 
				{
					ImageManager.requestImage( function(  v ) 
					{
						document.querySelector("#producto_edit_image").src = v.value.path;	
					})
				}

				static saved ( ) 
				{

					if( ManagerWindow.event == "create" )
					{
						let form = ManagerWindow.getForm( );

							let formulario = new FormData( );
								formulario.append( "nombre" , form.name.value );
								formulario.append( "descripcion" , form.text.value );
								formulario.append( "stock" , form.stock.value );
								formulario.append( "precio" , form.money.value );
								formulario.append( "imagen" , "" );
								formulario.append( "categoria" , 1 );
								formulario.append( "_token" , document.getElementsByName("csrf-token")[0].content );



							let ajax = new XMLHttpRequest( );	
								ajax.open( "POST" , "/admin/productos/create" , true );
								ajax.onreadystatechange = function( ) 
								{
								if( ajax.readyState == 4 ) 
								{
									location.reload();

								}
						
								}

								ajax.send( formulario );
					}else 
					{
							let form = ManagerWindow.getForm( );
							let pro  = ManagerWindow.actualProduct;
						
							pro.setPrecio( form.money.value );
							pro.setNombre( form.name.value  );
							pro.setStock ( form.stock.value );
							pro.setDescripcion( form.text.value );

							let formulario = new FormData( );
								formulario.append( "id" , pro.id );
								formulario.append( "nombre" , pro.nombre );
								formulario.append( "descripcion" , pro.descripcion );
								formulario.append( "stock" , pro.stock );
								formulario.append( "precio" , pro.precio );
								formulario.append( "imagen" , pro.imagen );
								formulario.append( "categoria" , pro.categoria );
								formulario.append( "_token" , document.getElementsByName("csrf-token")[0].content );



							let ajax = new XMLHttpRequest( );	
								ajax.open( "POST" , "/admin/productos/modify" , true );
								ajax.onreadystatechange = function( ) 
								{
								if( ajax.readyState == 4 ) 
								{
									let response = JSON.parse( ajax.responseText );
									
									if( !response.status ) alert( response.text );

								}
						
								}



								ajax.send( formulario );
							document.querySelector("#windowForm").style.display = "none";
					}
				}

			};



	
