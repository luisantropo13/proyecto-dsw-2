class Visor {
	static COLOR_FOOTER_ELEMENT = "rgba(255, 255, 255, 0.4)";
	static COLOR_RADIO_BUTTON = { default: "black", selected: "red" };
	static HEIGHT_FOOTER_ELEMENT = 70;
	static TRANSITION_TIME = "1s";

	constructor(width, height, images) {
		const ESTO = this;

		this.index = 0;
		this.imagesList = [];
		this.width = width;
		this.height = height;

		this.element = document.createElement("div");
		this.element.style.width = `${width}px`;
		this.element.style.height = `${height}px`;
		this.element.style.overflow = "hidden";

		this.imagescontainer = document.createElement("div"); this.element.appendChild(this.imagescontainer);
		this.imagescontainer.style.height = "100%";
		this.imagescontainer.style.width = "100%";
		this.imagescontainer.style.transition = Visor.TRANSITION_TIME;

		this.footercontainer = document.createElement("div"); this.element.appendChild(this.footercontainer);
		this.footercontainer.style.width = `${width}px`;
		this.footercontainer.style.height = `${Visor.HEIGHT_FOOTER_ELEMENT}px`;
		this.footercontainer.style.backgroundColor = "#ffffff2b";
		this.footercontainer.style.position = "absolute";
		this.footercontainer.style.marginTop = (-1 * Visor.HEIGHT_FOOTER_ELEMENT) + "px";
		this.footercontainer.style.display = "flex";
		this.footercontainer.style.justifyContent = "center";
		this.footercontainer.style.alignItems = "center";

		images.forEach((val) => ESTO.addImage(val));

		this.goImage(0);
	}

	goImage(index) {
		if (this.imagesList.length > index) {
			this.imagescontainer.style.marginLeft = (-1 * this.width * index) + "px";
			this.imagesList[this.index].button.style.backgroundColor = Visor.COLOR_RADIO_BUTTON.default;

			this.imagesList[(this.index = index)].button.style.backgroundColor = Visor.COLOR_RADIO_BUTTON.selected;
		}
	}

	addImage(ruta) {
		const ACTUAL_MAX_IMAGES = this.imagesList.length;

		let img = document.createElement("img"); this.imagescontainer.appendChild(img);
		img.src = "images/" + ruta;
		img.style.width = `${this.width}px`;
		img.style.height = `${this.height}px`;

		let button = document.createElement("div"); this.footercontainer.appendChild(button);
		button.style.backgroundColor = Visor.COLOR_RADIO_BUTTON.default;
		button.style.width = "20px"
		button.style.height = "20px";
		button.style.margin = "3px";
		button.style.borderRadius = "100%";
		button.style.cursor = "pointer";
		button.onclick = () => this.goImage(ACTUAL_MAX_IMAGES);

		this.imagesList.push(
			{
				img: img,
				button: button
			});

		this.imagescontainer.style.width = (this.imagesList.length * this.width) + "px";


	}


	getElement() {
		return this.element;
	}
};


window.addEventListener("load", function () {
	document.querySelector("#visor").appendChild(new Visor(700, 500,
		[
			"viewimage1.jpg",
			"viewimage2.jpg",
			"viewimage3.jpg"
		]).getElement());


	document.querySelector("#portada").style.height = (window.screen.height - 46) + "px";
});
