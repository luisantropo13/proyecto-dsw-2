

	<style>
			td
			{
				padding:8px;
			}

			tbody tr 
			{
				border-top: 1px solid black;
			}

			table
			{
				border-collapse:collapse;
				width:100%;
			}

			thead
			{
				font-weight:600;
			}		
	
			td
			{

				text-align:center;
			}
	</style>

	<div style="margin:8px;padding:8px;border-radius:8px;background-color:white;">
		<div style="margin-bottom:10px;display:flex;justify-content:space-between;">
			<div style="font-size:19px;font-weight:600;">Productos</div>
			<div>
				<button>Crear</button>
			</div>
		</div>

		<table>
				<thead>
					<tr>
						<td>ID</td>
						<td>Nombre</td>
						<td>Descripción</td>
						<td>Precio</td>
						<td>Categoría</td>
						<td></td>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td>0</td>
						<td>Ejemplo</td>
						<td>Esto es un ejemplo de un producto</td>
						<td>300€</td>
						<td>Bocadillos</td>
						<td>
							<input type="checkbox" />
						</td>

					</tr>

				</tbody>

		</table>
	</div>

