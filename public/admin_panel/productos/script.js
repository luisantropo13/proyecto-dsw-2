
			function createElement( ele , data ) 
			{
				let c = document.createElement( ele );

				if( typeof data == "object" ) 
					c.appendChild( data );
				else
					c.innerHTML = data;

				return c;
			}

			class Producto
			{
				constructor( data ) 
				{
					this.id     = data.id;
					this.nombre = data.nombre;
					this.precio = data.precio;
					this.stock  = data.stock;
					this.imagen = data.image;
					this.categoria = data.categoria;
					this.descripcion = data.descripcion;
				}

				getRow( ) 
				{
					const ESTO = this;
					let tr = document.createElement( "tr" );
					let button = createElement( "button" , "Editar" );
					    button.onclick = function( ) 
					    {
						ManagerWindow.editProducto( ESTO );	
					    }

					let tds = [
							this.td_id          = createElement( "td" , this.id ),
							this.td_nombre      = createElement( "td" , this.nombre ),
							this.td_descripcion = createElement( "td" , this.descripcion ),
							this.td_precio      = createElement( "td" , this.precio ),
							this.td_categoria   = createElement( "td" , this.categoria ),
							this.td_stock       = createElement( "td" , this.stock ),
							this.td_button      = createElement( "td" , button )
					];

					tds.forEach( ( val ) => tr.appendChild( val ) );

					return tr;
				}


				setNombre( val ) 
				{
					this.td_nombre.innerHTML = val;
					this.nombre              = val;
				}

				setDescripcion( val ) 
				{
					this.td_descripcion.innerHTML = val;
					this.descripcion              = val;
				}


				setPrecio( val ) 
				{
					this.td_precio.innerHTML = val;
					this.precio              = val;
				}


				setStock( val ) 
				{
					this.td_stock.innerHTML  = val;
					this.stock              = val;
				}

				static loadProductos ( ) 
				{
					fetch( "/menu/10/1,2")
						.then( (val) => val.json() )
						.then( function( j ) 
						{
							console.log( j );
						})
						.catch( (error) => alert(error) );
				}

			};

			class ManagerWindow
			{
				static actualProduct = null;

				static getForm( ) 
				{
					return  {
							money : document.querySelector("#producto_edit_money"),
							name  : document.querySelector("#producto_edit_nombre"),
							text  : document.querySelector("#producto_edit_text"),	
							stock : document.querySelector("#producto_edit_stock")	
					};

				}

				static editProducto( producto ) 
				{
					ManagerWindow.actualProduct = producto;
			
					let campos = ManagerWindow.getForm( );	

					campos.money.value = producto.precio;
					campos.name.value  = producto.nombre;
					campos.text.value  = producto.descripcion;
					campos.stock.value = producto.stock;

					document.querySelector("#windowForm").style.display = "flex";
				}

				static clickImage( image ) 
				{
					ImageManager.requestImage( function(  v ) 
					{
						console.log( v );
					})
				}

				static saved ( ) 
				{
					let form = ManagerWindow.getForm( );
					let pro  = ManagerWindow.actualProduct;
				
					let formulario = new FormData( );
					    formulario.append( "id" , pro.id );
					    formulario.append( "nombre" , pro.nombre );
					    formulario.append( "descripcion" , pro.descripcion );
					    formulario.append( "stock" , pro.stock );
					    formulario.append( "precio" , pro.precio );
					    formulario.append( "imagen" , pro.imagen );
					    formulario.append( "categoria" , pro.categoria );


					pro.setPrecio( form.money.value );
					pro.setNombre( form.name.value  );
					pro.setStock ( form.stock.value );
					pro.setDescripcion( form.text.value );

					let ajax = new XMLHttpRequest( );	
					    ajax.open( "POST" , "/formProducto" , true );
					    ajax.onreadystatechange = function( ) 
					    {
						if( ajax.readyState == 4 ) 
						{
							let response = JSON.parse( ajax.responseText );
							
							if( response.status ) alert( response.text );

						}
				
					    }



					    ajax.send( formulario );
					document.querySelector("#windowForm").style.display = "none";
					  
				}

			};



		Producto.loadProductos( );
