
	<script>

B	

	</script>


	<style>
			td
			{
				padding:8px;
			}

			tbody tr 
			{
				border-top: 1px solid black;
			}

			table
			{
				border-collapse:collapse;
				width:100%;
			}

			thead
			{
				font-weight:600;
			}		
	
			td
			{

				text-align:center;
			}

			.buttonQuit
			{
background-color: red;
height: 30px;
width: 30px;
color: white;
font-family: cursive;
border: 0;
			}
	</style>

	<div style="margin:8px;padding:8px;border-radius:8px;background-color:white;">
		<div style="margin-bottom:10px;display:flex;justify-content:space-between;">
			<div style="font-size:19px;font-weight:600;">Productos</div>
			<div>
				<button onclick="ManagerWindow.saved();">Crear</button>
			</div>
		</div>

		<table>
				<thead>
					<tr>
						<td>ID</td>
						<td>Nombre</td>
						<td>Descripción</td>
						<td>Precio</td>
						<td>Categoría</td>
						<td>Stock</td>
						<td></td>
					</tr>
				</thead>

				<tbody id="tableData">
			
				</tbody>

		</table>
	</div>


	<div style="position:fixed;top:0;left:0;width:100%;background-color:#00000096;height:100%;justify-content:center;display:none;padding-top:40px;" id="windowForm">

			<div style="width:540px;height:400px;background-color:white;">
				<div style="background-color:#0000ff91;color:white;font-size:14px;font-weight:600;display:flex;height:35px;align-items:center;padding:5px;justify-content:space-between;">
					<div>Modificar Producto</div>
					<div>
						<button class="buttonQuit" onclick="document.querySelector('#windowForm').style.display = 'none';">x</button>
					</div>

				</div>

				<div style="display:flex;padding:8px;">
					<div>
						<img onclick="ManagerWindow.clickImage( this );" src="https://img-global.cpcdn.com/recipes/eca56cc2e88b6f7d/1200x630cq70/photo.jpg" style="width:170px;height:170px;border:1px solid black;" />

						<div style="display:flex;align-items:center;justify-content:center;flex-direction:column;margin-bottom:10px;margin-top:10px;">
							<div style="border:1px solid gray;height:25px;width:150px;display:flex;margin-bottom:10px;">
								<input id="producto_edit_money" style="text-align:right;border:0px;width:86px;" value="0" />
								<div style="display:flex;align-items:center;font-size:15px;">€</div>
							</div>	

							<div style="border:1px solid gray;height:25px;width:150px;display:flex;">
								<div>Stock</div>
								<input style="text-align:right;border:0px;width:86px;" id="producto_edit_stock" value="0" />
							</div>	
			
						</div>

						<fieldset style="margin-bottom:10px;">
								<legend>Categoría</legend>
								<div>
									<select style="background-color:white;border:0;appearance:none;">
											@foreach( $categorias as $categoria) 
												<option value="{{$categoria->id}}">{{$categoria->name}}</option>	
											@endforeach
									</select>
								</div>
						</fieldset>

				<div style="text-align:center;"><button onclick="ManagerWindow.saved();">Guardar</button></div>

					</div>
					
					<div style="text-align:center;flex:1;">
							<div>
								<div style="margin-bottom:5px;font-weight:600;">Nombre del producto</div>
								<div>
										<input id="producto_edit_nombre" style="width:70%;" />	
								</div>
							</div>

							<div style="margin-top:10px;">
								<div style="font-weight:600;margin-bottom:5px;">Descripción del producto</div>
								<div>
										<textarea style="width:303px;height:215px; resize: none; " id="producto_edit_text"></textarea>	
								</div>
							</div>




					</div>

				</div>



			</div>


	</div>
