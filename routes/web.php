<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\PoliticaController;
use App\Http\Controllers\FormLRController;
use App\Models\Menu;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Request;
use GuzzleHttp\Psr7\Response;
use App\Http\Controllers\PDFController;

Route::get('generate-pdf', [PDFController::class, 'generatePDF']);

//Vistas
Route::view('/', "home" );
Route::get('/menu/{cat?}', [ MenuController::class , "show" ] );
Route::get('/legal'      , [ PoliticaController::class , "show" ] );
Route::get('/nosotros'   , [ ContactController::class , "show" ]);




Route::get( "/register" , function() { return view( "register");} );
Route::post("/register" , [ FormLRController::class , "register" ] );

Route::get( "/login" , function( ) { return view( "login" ); } );
Route::post( "/login" , [ FormLRController::class , "login" ] );





Route::get("/admin/index"    , [ AdminController::class , "show_index"] );
Route::get("/admin/productos" , [AdminController::class , "show_productos"] );
Route::view("/admin/usuarios" , "admin.usuarios" );
Route::post("/admin/images" , [ AdminController::class , "getAllImagenes"]);

Route::post("/admin/productos/modify" , [AdminController::class , "modify_producto"] );
Route::post("/admin/productos/create" , [AdminController::class , "create_producto" ]);
Route::get("/admin/productos/delete/{id}" , [ AdminController::class , "delete_producto" ]);