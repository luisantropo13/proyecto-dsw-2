<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserModel;
class FormLRController extends Controller
{
    private static  function createSession( $user  ) 
    {

        session( [ "user" => 
        [
                "nombre" =>$user
        ]]);
    }

    public function login( $user , $password )
    {
            if( UserModel::login( $user , $password ) ) 
            {
                self::createSession( $user );    
            }else
            {
                echo "Incorrecto";
            }
    }

    public function register( Request $request ) 
    {
        if( $request->has("usuario") && $request->has("email") &&  $request->has("telf") && $request->has("pass1") && $request->has("pass2") )
        {
            if( $request->pass1 != $request->pass2 ) 
            {
                    echo "Los campos de contraseña no son iguales.";
            }else
            {
                UserModel::register( $request->usuario , $request->email , $request->pass1 , $request->telf );
                echo "Te has registrado con éxito";


                self::createSession(  $request->usuario );

                return redirect( "/");

            }
        }
    }
}
