<?php

namespace App\Http\Controllers;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Models\MenuModel;
use Illuminate\Support\Facades\DB;
use App\Models\Producto;

class AdminController extends Controller
{
    public static function getAllImagenes(){
        return Admin::getImages();
    }

    public function show_productos( ) 
    {
        return view( "admin.productos" , 
        [
                "categorias" => MenuModel::loadCategorias(),
                "productos"  => MenuModel::loadProductos( )
        ]);
    }


    public function show_index( ) 
    {
        return view( "admin.index" , 
        [
                "data" => Admin::getDataInicio()
        ]);
    }

    public function delete_producto( $id ) 
    {
        Producto::removeProducto( $id );
    }

    public function create_producto( Request $request ) 
    {
            Producto::createProducto([
                "nombre" => $request->nombre,
                "fk_foto" => 1 ,
                "fk_categoria" => 1 , 
                "precio" => $request->precio,
                "stock"  => $request->stock,
                "descripcion" => $request->descripcion
            ]);
    }

    public function modify_producto( Request $request ) 
    {
            DB::table( "productos" )
                ->where( "id" , $request->id)
                ->update
                ([ 
                    "nombre" => $request->nombre,
                    "description" => $request->descripcion,
                    "stock"       => $request->stock,
                    "precio"      => $request->precio,
                    "fk_categoria"   => $request->categoria    

                ]);

                return [ "status" => true , "text" => "ok" ];
    }
}
