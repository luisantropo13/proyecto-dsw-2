<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Politica;

class PoliticaController extends Controller
{
    public function show( ) 
    {
            return view( "politica", [
                        "data" => Politica::loadPolitica( )
             ]);
    }
    
}
