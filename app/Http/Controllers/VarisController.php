<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class VarisController extends Controller
{
    public static function getValueById(String $id, String $value_default = ""){
        $data = DB::table('varis')->get()->where("clave",$id)->first();
        if($data!=null){
            return $data->valor;
        }
        return $value_default;
    }
}
