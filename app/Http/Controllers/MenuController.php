<?php

namespace App\Http\Controllers;
use App\Models\MenuModel;
use Illuminate\Http\Request;

class MenuController extends Controller
{

    public static function show( $cat = null) 
    {

        
            return view( "menu" , [
                    "productos"  => MenuModel::loadProductos(  $cat),
                    "categorias" => MenuModel::loadCategorias( )
            ]);
    }
    
}


