<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Producto extends Model
{
    use HasFactory;


    public static function createProducto( $data ) 
    {
        DB::insert( "insert into productos ( nombre , fk_foto , fk_categoria , precio , stock , description  ) values ( ? , ? , ? , ? , ? , ?   ) ", [
            $data["nombre"] , 
            $data["fk_foto"], 
            $data["fk_categoria"] ,
             $data["precio"] , 
             $data["stock"] ,
              $data["descripcion"]
    ]);
    }

    public static function removeProducto( $id ) 
    {
        Producto::find( $id )->delete();
    }
}
