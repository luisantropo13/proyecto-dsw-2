<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    use HasFactory;

    public static function getImages(){
        return DB::table('fotos')->get();
    }

    public static function getDataInicio( ) 
    {
        return [
                "num_productos" => DB::table("productos")->count() , 
                "num_usuarios"  => DB::table("clientes")->count()  , 
                "num_categorias" => DB::table("categorias")->count() 
        ];
    }
}
