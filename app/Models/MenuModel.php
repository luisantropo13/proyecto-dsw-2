<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Producto;

class MenuModel extends Model
{
    use HasFactory;


    public static function loadProductos(  $cat = null )
    {
             $pro = DB::table( "productos" );

                 if( $cat != null ) 
                 $pro = DB::table( "productos")->where( "fk_categoria" , $cat );
     
           
                   return $pro->join( "fotos" , "fotos.id" , "=" , "productos.fk_foto" )
                                ->select( "productos.id" , "fotos.ruta as imagen" , "productos.precio" , "productos.stock" , 
                                          "productos.nombre" , "productos.description" ,  "productos.fk_categoria" )
                                ->get( );
    }

    public static function loadCategorias( ) 
    {
        return DB::table( "categorias")->get( );
    }
}
