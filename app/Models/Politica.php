<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Politica extends Model
{
    use HasFactory;

    public static function loadPolitica( ) 
    {
            return DB::table( "politicas" )->get( );
    }
}
